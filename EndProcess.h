#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <string.h>
#include <vector>
#include <fstream>

using namespace std;

int main(int argc, char* args[]);

const int max_args = 3;
const int min_args = 2;
const string close = "close";
const string list = "list";

class ProcessGetter
{
		PROCESSENTRY32 *pentry32;
		PROCESSENTRY32 s_entry32;
		HANDLE processSnapshot;
	public:
		ProcessGetter();
		PROCESSENTRY32* getProcess(string name);
		vector<string> listProcesses();
		bool endProcess(PROCESSENTRY32* p);
};