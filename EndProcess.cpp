#include "EndProcess.h"
/* Arguments:
	args[1] = name of process
*/
int main(int argc, char* args[])
{	
	ofstream out("debug.txt", ofstream::out);
	out.write("hurdur", 10);
	out.close();
	//Check if the argument amount is correct
	if(argc > max_args || argc < min_args) return 0;
	string cmd(args[1]);
	//Check if the given arguments are valid
	if(cmd.compare(list) != 0 && cmd.compare(close) != 0) return 0;
	//Is a second argument needed?
	string argument;
	if(argc == max_args) argument = args[2];
	ProcessGetter p;
	if(cmd == close) 
	{
		ofstream out("close.txt", ofstream::out);
		PROCESSENTRY32* process = p.getProcess(argument);
		const char* msg;
		cout << process->szExeFile << endl;
		if(p.endProcess(process)) msg = "succeeded";
		else msg = "failed";
		cout << msg << endl;
		out.write(msg, strlen(msg));
		out.write("\n", 1);
		out.close();
	}
	if(cmd == list)
	{
		ofstream out("list.txt", ofstream::out);
		vector<string> vec = p.listProcesses();
		int len = vec.size();
		for(int i = 0; i < len; i++)
		{
			cout << vec[i] << endl;
			int size = vec[i].size();
			out.write(vec[i].c_str(), size);
			out.write("\n", 1);
		}
		out.close();
	}
	return 1;
}

ProcessGetter::ProcessGetter()
{
	s_entry32.dwSize = sizeof(PROCESSENTRY32);
	pentry32 = &s_entry32;
	processSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
}

//Returns the default system process if a process by name was not found
PROCESSENTRY32* ProcessGetter::getProcess(string name)
{
	Process32First(processSnapshot, pentry32);
	//Loop through every process and check for a match
	while(Process32Next(processSnapshot, pentry32) != FALSE)
	{
		string pname(pentry32->szExeFile);
		if(pname == name) return pentry32;
	}
	Process32First(processSnapshot, pentry32);
	return pentry32;
}

//Returns every running process in a vector
vector<string> ProcessGetter::listProcesses()
{
	Process32First(processSnapshot, pentry32);
	vector<string> result;
	while(Process32Next(processSnapshot, pentry32) != FALSE)
	{
		string name(pentry32->szExeFile);
		result.push_back(name);
	}
	return result;
}

//Ends process p if possible
bool ProcessGetter::endProcess(PROCESSENTRY32* p)
{
	DWORD pid = p->th32ProcessID;
	HANDLE phandle = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
	return TerminateProcess(phandle, 1);
}